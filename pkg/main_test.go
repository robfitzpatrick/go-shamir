package shamir

import (
	"bytes"
	"crypto/rand"
	"gitlab.com/robfitzpatrick/go-shamir/pkg/shamir"
	"testing"
)

func TestBasic(t *testing.T) {

	secretBytesToShare := []byte{0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F}

	const numShares = 5
	const threshold = 3

	shares := shamir.shareShamir(secretBytesToShare, threshold, numShares)

	sharesToUseInReconstruction := []int{0, 1, 2}

	sharesForReconstruction := []shamir.shamirFragment{
		shares[sharesToUseInReconstruction[0]],
		shares[sharesToUseInReconstruction[1]],
		shares[sharesToUseInReconstruction[2]],
	}

    reconstructedBytes := shamir.reconstructShamir(sharesForReconstruction)

    if !bytes.Equal(reconstructedBytes, secretBytesToShare) {
    	t.Fatalf("Bad reconstruction\n")
	}

}

func TestBasic_large(t *testing.T) {

	numBytesToShare := 1024 * 16384
	secretBytesToShare := make([]byte, numBytesToShare)

	_, randErr := rand.Read(secretBytesToShare)
	if nil != randErr {
		t.Fatalf("randErr: %v\n", randErr.Error())
	}

	const numShares = 5
	const threshold = 3


	shares := shamir.shareShamir(secretBytesToShare, threshold, numShares)

	sharesToUseInReconstruction := []int{0, 1, 2}

	sharesForReconstruction := []shamir.shamirFragment{
		shares[sharesToUseInReconstruction[0]],
		shares[sharesToUseInReconstruction[1]],
		shares[sharesToUseInReconstruction[2]],
	}

	reconstructedBytes := shamir.reconstructShamir(sharesForReconstruction)

	if !bytes.Equal(reconstructedBytes, secretBytesToShare) {
		t.Fatalf("Bad reconstruction\n")
	}

}

func TestBasic_bad(t *testing.T) {

	secretBytesToShare := []byte{0x09}

	const numShares = 5
	const threshold = 3

	shares := shamir.shareShamir(secretBytesToShare, threshold, numShares)

	sharesToUseInReconstruction := []int{0, 1, 0}

	sharesForReconstruction := []shamir.shamirFragment{
		shares[sharesToUseInReconstruction[0]],
		shares[sharesToUseInReconstruction[1]],
		shares[sharesToUseInReconstruction[2]],
	}

	reconstructedBytes := shamir.reconstructShamir(sharesForReconstruction)

	if bytes.Equal(reconstructedBytes, secretBytesToShare) {
		t.Fatalf("Should have failed\n")
	}

}

func TestBasic_notEnoughShares(t *testing.T) {

	secretBytesToShare := []byte{0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F}

	const numShares = 5
	const threshold = 3

	shares := shamir.shareShamir(secretBytesToShare, threshold, numShares)

	sharesToUseInReconstruction := []int{0, 1}

	sharesForReconstruction := []shamir.shamirFragment{
		shares[sharesToUseInReconstruction[0]],
		shares[sharesToUseInReconstruction[1]],
	}

	reconstructedBytes := shamir.reconstructShamir(sharesForReconstruction)

	if bytes.Equal(reconstructedBytes, secretBytesToShare) {
		t.Fatalf("Should have failed\n")
	}

}

