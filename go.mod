module gitlab.com/robfitzpatrick/go-shamir

go 1.15

require (
	github.com/carbocation/interpose v0.0.0-20161206215253-723534742ba3
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/viper v1.7.1
	github.com/tylerb/graceful v1.2.15
)
